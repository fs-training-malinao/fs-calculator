// Parent component

import './App.css';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import {Container} from "react-bootstrap";
import {Form, Row, Col, Button} from "react-bootstrap";
import {useState, useEffect} from "react";

function App() {

  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);
  const [isDisabled, setIsDisabled] = useState(false);

  const [operation, setOperation] = useState('none');
  const [output, setOutput] = useState(0);

  useEffect(() => {

      document.getElementById('plus').onclick = function(e) {
       e.preventDefault();
       setOutput(parseFloat(num1)+parseFloat(num2))
      
      }
      
      document.getElementById('minus').onclick = function(e) {
       e.preventDefault();
       setOutput(parseFloat(num1)-parseFloat(num2))
      
      }

      document.getElementById('times').onclick = function(e) {
       e.preventDefault();
       setOutput(parseFloat(num1)*parseFloat(num2))
      
      }

      document.getElementById('divide').onclick = function(e) {
       e.preventDefault();
       setOutput(parseFloat(num1)/parseFloat(num2))
      
      }

      document.getElementById('reset').onclick = function(e) {
       e.preventDefault();
       setNum1(0);
       setNum2(0);
       setOutput(0);
      
      }
  }, [num1, num2]);

  return (
    <> 
    <Container>

    <Form>
      <Row className="justify-content-center">

      <center>
      <h1 className='mt-5'>Calculator</h1>
      <br/>
      <h1>{output}</h1>
      <Form.Group className="mb-1 mt-5">
         <Form.Label></Form.Label>
        <Form.Control id='num1' type="number" value={num1}  onChange={(e) => setNum1(e.target.value)} placeholder='Number A'/>
      </Form.Group> 

      <Form.Group className="mb-5">
         <Form.Label></Form.Label>
        <Form.Control id='num2' type="number" value={num2} onChange={(e) => setNum2(e.target.value)} placeholder='Number B'/>
      </Form.Group> 
      </center>

      <Button id='plus' className='me-3'variant="primary" type="submit" 
       disabled={isDisabled}>Add</Button>
      <Button id='minus' className='me-3'variant="primary" type="submit" 
       disabled={isDisabled}>Subtract</Button>
      <Button id='times' className='me-3'variant="primary" type="submit" 
       disabled={isDisabled}>Multiply</Button>
      <Button id='divide' className='me-3' variant="primary" type="submit" 
       disabled={isDisabled}>Divide</Button>

       <Button id='reset' variant="primary" type="reset">Reset</Button>

      </Row>

      
      

    </Form> 
    </Container>
    </>
  );
}

export default App;

// Rendering process of calling or invoking our function
// Mounting is to display into the webpage 